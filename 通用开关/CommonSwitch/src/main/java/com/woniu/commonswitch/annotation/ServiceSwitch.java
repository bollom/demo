package com.woniu.commonswitch.annotation;

import com.woniu.commonswitch.constant.Constant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 通用开关注解 0 关  1 开
 * 程序员蜗牛
 * </p>
 */
@Target({ElementType.METHOD})  // 作用在方法上
@Retention(RetentionPolicy.RUNTIME)  // 运行时起作用
public @interface ServiceSwitch {

	/**
	 * 业务开关的key（不同key代表不同功效的开关）
	 * {@link Constant.ConfigCode}
	 */
	String switchKey();

	// 提示信息，默认值可在使用注解时自行定义。
	String message() default "当前请求人数过多，请稍后重试。";
}
