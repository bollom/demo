package com.woniu.pos.common.mutidatesource;

/**
 * @author 蜗牛
 */
public interface DSEnum {

    String DATA_SOURCE_CORE = "dataSourceCore";        //核心数据源

    String DATA_SOURCE_BIZ = "dataSourceBiz";           //其他业务的数据源
}
