package com.woniu.pos.common.annotion;

import java.lang.annotation.*;

/**
 * 多数据源标识
 * @author 蜗牛
 */

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface DataSource {

    String name() default "";
}
