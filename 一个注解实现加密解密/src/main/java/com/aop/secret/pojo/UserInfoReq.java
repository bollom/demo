package com.aop.secret.pojo;

import lombok.Data;


@Data
public class UserInfoReq {
	private String name;
	private Integer age;
	private String idCard;
	private String phone;
	private String address;
}
