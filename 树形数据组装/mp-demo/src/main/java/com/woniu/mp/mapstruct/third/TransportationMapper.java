package com.woniu.mp.mapstruct.third;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;


@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface TransportationMapper {
    /**
     * 类共用属性，如何复用
     */
    @ToEntity
    @Mapping( target = "brandName", source = "brand")
    Bike map(BikeDto source);

    @ToEntity
    @Mapping( target = "otherName", source = "other")
    Car map(CarDto source);
}
