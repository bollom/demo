package com.woniu.mp.mapper;


import com.woniu.mp.mapstruct.first.Source;
import com.woniu.mp.mapstruct.first.SourceTargetMapper;
import com.woniu.mp.mapstruct.first.Target;

import com.woniu.mp.mapstruct.second.Customer;
import com.woniu.mp.mapstruct.second.CustomerDto;
import com.woniu.mp.mapstruct.second.CustomerMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Date;

@SpringBootTest
class TestTransactionTest {
    @Resource
    TestTransaction testTransaction;

    @Resource
    SourceTargetMapper targetMapper;

    @Resource
    CustomerMapper customerMapper;



    /**
     * 常量转换
     */
    @Test
    void testFirst() {
        Source source = new Source();
        source.setLongProp(1l);
        source.setStringProp("woniu");
        Target target = targetMapper.sourceToTarget(source);
        System.out.println(target);
    }

    /**
     * 转换中调用表达式
     */
    @Test
    void testSecond() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setTime(new Date());
        customerDto.setCustomerName("woniu");
        customerDto.setFormat("yyyy-MM");
        Customer customer = customerMapper.toCustomer(customerDto);
        System.out.println(customer);
    }

    @Test
    void testThird() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setTime(new Date());
        customerDto.setCustomerName("woniu");
        customerDto.setFormat("yyyy-MM");
        Customer customer = customerMapper.toCustomer(customerDto);
        System.out.println(customer);
    }



}
