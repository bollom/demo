package com.woniu.pos.common.dao.repository;

import com.woniu.pos.common.dao.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 蜗牛
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
