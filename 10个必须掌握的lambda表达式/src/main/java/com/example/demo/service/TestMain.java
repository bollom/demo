package com.example.demo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//简化你的代码，提高生产力：这10个Lambda表达式必须掌握
public class TestMain {

    public static void main(String[] args) {
//        1.  使用Lambda表达式进行集合遍历
//        List<String> list = Arrays.asList("apple", "banana", "orange");
//        for (String fruit : list) {
//            System.out.println(fruit);
//        }
//
//        List<String> list2 = Arrays.asList("apple", "banana", "orange");
//        list2.forEach(fruit -> System.out.println(fruit));



//        2.  使用Lambda表达式进行排序
//        List<String> list2 = Arrays.asList("apple", "banana", "orange");
//        Collections.sort(list2, new Comparator<String>() {
//            public int compare(String s1, String s2) {
//                return s1.compareTo(s2);
//            }
//        });
//
//        List<String> list = Arrays.asList("apple", "banana", "orange");
//        Collections.sort(list, (s1, s2) -> s1.compareTo(s2));






//        3.  使用Lambda表达式进行过滤
//        List<String> list = Arrays.asList("apple", "banana", "orange");
//        List<String> filteredList = new ArrayList<String>();
//        for (String fruit : list) {
//            if (fruit.startsWith("a")) {
//                filteredList.add(fruit);
//            }
//        }
//
//        List<String> list2 = Arrays.asList("apple", "banana", "orange");
//        List<String> filteredLists =  list2.
//                stream().
//                filter(fruit -> fruit.startsWith("a")).
//                collect(Collectors.toList());


//        4.  使用Lambda表达式进行映射
//        List<String> list = Arrays.asList("apple", "banana", "orange");
//        List<Integer> lengths = new ArrayList<Integer>();
//        for (String fruit : list) {
//            lengths.add(fruit.length());
//        }
//
//        List<String> lists = Arrays.asList("apple", "banana", "orange");
//        List<Integer> lengthss = lists.stream().map(fruit -> fruit.length())
//                .collect(Collectors.toList());




//        5.  使用Lambda表达式进行归约
//        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
//        int sum = 0;
//        for (int i : list) {
//            sum += i;
//        }
//
//        List<Integer> list2 = Arrays.asList(1, 2, 3, 4, 5);
//        int sum2 = list2.stream().reduce(0, (a, b) -> a + b);







//        6.  使用Lambda表达式进行分组
//        List<String> list = Arrays.asList("apple", "banana", "orange");
//        Map<Integer, List<String>> grouped = new HashMap<Integer, List<String>>();
//        for (String fruit : list) {
//            int length = fruit.length();
//            if (!grouped.containsKey(length)) {
//                grouped.put(length, new ArrayList<String>());
//            }
//            grouped.get(length).add(fruit);
//        }
//
//
//        List<String> lists = Arrays.asList("apple", "banana", "orange");
//        Map<Integer, List<String>> groupeds = lists.stream().collect(Collectors.groupingBy(fruit -> fruit.length()));




//        7.  使用Lambda表达式进行函数式接口的实现
//        MyInterface myObject = new MyInterface() {
//            public void doSomething(String input) {
//                System.out.println(input);
//            }
//        };
//        myObject.doSomething("Hello World");
//
//        MyInterface myObject = input -> System.out.println(input);
//        myObject.doSomething("Hello World");







        //8.  使用Lambda表达式进行线程的创建
//        Thread thread = new Thread(new Runnable() {
//            public void run() {
//                System.out.println("Thread is running.");
//            }
//        });
//        thread.start();
//
//        Thread threads = new Thread(() -> System.out.println("Thread is running."));
//        threads.start();





//        9.  使用Lambda表达式进行Optional的操作
//        String str = "Hello World";
//        if (str != null) {
//            System.out.println(str.toUpperCase());
//        }
//
//        Optional<String> strs = Optional.ofNullable("Hello World");
//        strs.map(String::toUpperCase).ifPresent(System.out::println);






//        10.  使用Lambda表达式进行Stream的流水线操作

        List<String> list = Arrays.asList("apple", "banana", "orange");
        List<String> filteredList = new ArrayList<String>();
        for (String fruit : list) {
            if (fruit.startsWith("a")) {
                filteredList.add(fruit.toUpperCase());
            }
        }
        Collections.sort(filteredList);

        List<String> lists = Arrays.asList("apple", "banana", "orange");
        List<String> filteredLists = lists.stream().filter(fruit -> fruit.startsWith("a")).map(String::toUpperCase).sorted().collect(Collectors.toList());


    }
}

