package com.woniu.service;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Component
public class RedisIdempotentStorage implements IdempotentStorage {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void save(String idempotentId) {
        stringRedisTemplate.opsForValue().set(idempotentId, idempotentId, 10, TimeUnit.MINUTES);
    }

    @Override
    public boolean delete(String idempotentId) {
        return stringRedisTemplate.delete(idempotentId);
    }
}