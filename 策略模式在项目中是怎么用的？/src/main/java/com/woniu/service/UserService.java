package com.woniu.service;

import com.woniu.dao.master.MasterUserDao;
import com.woniu.dao.slave.SlaveUserDao;
import com.woniu.vo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserService {


    @Autowired
    private MasterUserDao masterUserDao;

    @Autowired
    private SlaveUserDao slaveUserDao;

    @Test
    public void testUser() {
        User user = new User();
        user.setId(1l);
        user.setSex(1);
        user.setName("蜗牛");
        masterUserDao.insertUser(user);
        slaveUserDao.insertUser(user);
    }


}
