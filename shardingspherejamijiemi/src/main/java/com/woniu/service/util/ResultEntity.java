package com.woniu.service.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.woniu.service.enums.ResponseCodeEnum;
/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
public class ResultEntity<T> {

    private String code;

    private String msg;

    private T data;

    public ResultEntity(){}

    public ResultEntity(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public ResultEntity(String code, String msg, T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return ResponseCodeEnum.SUCCESS.getCode().equals(this.getCode());
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static ResultEntity fail(String code, String msg) {
        return new ResultEntity(code, msg);
    }

    public static <T> ResultEntity fail(String code, String msg, T data) {
        return new ResultEntity(code, msg, data);
    }

    public static ResultEntity ok(String code, String msg) {
        return new ResultEntity(code, msg);
    }

    public static <T> ResultEntity ok(String code, String msg, T data) {
        return new ResultEntity(code, msg, data);
    }

    public static ResultEntity ok(String msg) {
        return new ResultEntity(ResponseCodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> ResultEntity ok(String msg, T data) {
        return new ResultEntity(ResponseCodeEnum.SUCCESS.getCode(), msg, data);
    }

    public static ResultEntity fail(String msg) {
        return new ResultEntity(ResponseCodeEnum.FAIL.getCode(), msg);
    }
}
