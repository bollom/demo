package com.woniu.dao;

import com.woniu.vo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserDao {

    int insertUser(User user);

    List<User> selectUser(User user);
}
